# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="A better repoman!"
HOMEPAGE="http://projects.eroyf.org/reprehendo/"
SRC_URI="http://projects.eroyf.org/${PN}/${P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=">=sys-apps/paludis-0.26"
RDEPEND="${DEPEND}"

src_install(){
	make install DESTDIR="${D}"
}
