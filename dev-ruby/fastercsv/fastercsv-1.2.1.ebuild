# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit ruby

DESCRIPTION="Replacement for ruby's standard csv parser"
HOMEPAGE="http://fastercsv.rubyforge.org/"
SRC_URI="http://rubyforge.org/frs/download.php/25598/fastercsv-1.2.1.tgz"
KEYWORDS="~amd64 ~x86"
IUSE="examples"
LICENSE="GPL-2"

DEPEND="virtual/ruby"
RDEPEND="virtual/ruby"

src_compile() {
	ruby setup.rb config || die "config failed"
	ruby setup.rb setup || die "setup failed"
}

src_install() {
	ruby setup.rb install --prefix="${D}" || die "install failed"

	if use examples; then
		dodir /usr/share/doc/${PF}/examples
		cp examples/*.rb "${D}/usr/share/doc/${PF}/examples" || die "cp examples failed"
	fi

	dodoc CHANGELOG README TODO
}

src_test() {
	ruby -I lib:test test/ts_all.rb
}

