# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit ruby gems

DESCRIPTION="Simple mock object library for Ruby unit testing"
HOMEPAGE="http://onestepback.org/software/flexmock/"
SRC_URI="http://gems.rubyforge.org/gems/${P}.gem"

LICENSE="as-is"
SLOT="${PV}"
KEYWORDS="~amd64 ~x86"
IUSE=""

USE_RUBY="ruby18 ruby19"

src_test() {
	gems_location
	elog "Tests can be run with dev-ruby/rake installed"
	elog "Please run"
	elog "cd ${GEMSDIR}/gems/${P} && rake test"
}
