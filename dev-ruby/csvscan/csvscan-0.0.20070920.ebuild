# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit ruby

DESCRIPTION="Fastest ruby csv parser"
HOMEPAGE="http://raa.ruby-lang.org/project/csvscan/"
SRC_URI="http://www.moonwolf.com/ruby/archive/${P}.tar.gz"
KEYWORDS="~amd64 ~x86"
LICENSE="ruby"
IUSE=""

DEPEND="virtual/ruby"
RDEPEND="virtual/ruby"

src_compile() {
	ruby setup.rb config || die "config failed"
	ruby setup.rb setup || die "setup failed"
}

src_install() {
	ruby setup.rb install --prefix="${D}" || die "install failed"
	doins README.ja
}

