# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-ruby/ruby-mmap/ruby-mmap-0.2.6.ebuild,v 1.1 2007/01/08 21:44:26 twp Exp $

inherit ruby

IUSE="cjk"
EAPI="paludis-1"

MY_P=${P/ruby-/}

DESCRIPTION="The Mmap class implement memory-mapped file objects"
HOMEPAGE="http://moulon.inra.fr/ruby/mmap.html"
SRC_URI="ftp://moulon.inra.fr/pub/ruby/${MY_P}.tar.gz"

SLOT="0"
LICENSE="Ruby"
KEYWORDS="~alpha ~amd64 ~ia64 ~ppc ~sparc ~x86"

S=${WORKDIR}/${MY_P}

DEPEND="cjk? ( dev-lang/ruby[cjk] )
	!cjk? ( dev-lang/ruby[-cjk] )"

src_unpack() {
	unpack ${A}

	if built_with_use dev-lang/ruby cjk; then
		cd "${S}"
		epatch "${FILESDIR}/ruby-mmap-0.2.6-oniguruma_rb_reg_regsub.patch"
	fi
}

src_compile() {
	ruby_src_compile all rdoc || die
}
