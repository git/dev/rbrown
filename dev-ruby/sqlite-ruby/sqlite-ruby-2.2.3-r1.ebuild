# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-ruby/sqlite-ruby/sqlite-ruby-2.2.3-r1.ebuild,v 1.9 2007/03/15 03:42:28 tgall Exp $

inherit ruby gems

DESCRIPTION="An extension library to access a SQLite database from Ruby"
HOMEPAGE="http://rubyforge.org/projects/sqlite-ruby/"
LICENSE="BSD"
EAPI="paludis-1"

SRC_URI="http://gems.rubyforge.org/gems/${P}.gem"

KEYWORDS="amd64 ia64 ppc ~ppc-macos ~ppc64 sparc x86"
SLOT="0"
IUSE=""

USE_RUBY="ruby18 ruby19"
DEPEND="dev-db/sqlite:2"
