# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

inherit perl-module

DESCRIPTION="Cross-platform path specification manipulation"
SRC_URI="mirror://cpan/authors/id/K/KW/KWILLIAMS/${P}.tar.gz"
HOMEPAGE="http://search.cpan.org/~kwilliams/${P}/"

SLOT="0"
LICENSE="Artistic"
KEYWORDS="~amd64"
IUSE=""

SRC_TEST="do"

DEPEND="dev-lang/perl
        >=virtual/perl-File-Spec-0.87
        virtual/perl-Test-Simple"
